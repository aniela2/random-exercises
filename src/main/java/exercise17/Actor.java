package exercise17;

import java.time.LocalDate;
import java.util.Objects;

public record Actor(String name, Gender gender, LocalDate dateOfBirth) {
    public Actor(String name, Gender gender, LocalDate dateOfBirth) {
        if (Objects.isNull(name)) {
            throw new RuntimeException();
        }
        this.name = name;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
    }
}
