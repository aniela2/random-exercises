package exercise17;

import java.awt.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

//(attributes: name - mandatory, releaseYear - mandatory, imdbRate, MovieType(enum),
//    // actors - mandatory (List of Actors (attributes: name -mandatory, Gender(enum), dateOfBirth)))
public record Movie(String name, Integer releaseYear, Double imdbRate, MovieType movieType, List<Actor> actors) {
    public Movie(String name, Integer releaseYear, Double imdbRate, MovieType movieType, List<Actor> actors) {
        if (Objects.isNull(name) || Objects.isNull(releaseYear) || Objects.isNull(actors) || actors.isEmpty()) {
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.releaseYear = releaseYear;
        this.imdbRate = imdbRate;
        this.movieType = movieType;
        this.actors = List.copyOf(actors);
    }
}
