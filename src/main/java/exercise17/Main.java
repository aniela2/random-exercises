package exercise17;

import ro.aniela.exercise54.General;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    //Given a Set of Movies (attributes: name - mandatory, releaseYear - mandatory, imdbRate, MovieType(enum),
    // actors - mandatory (List of Actors (attributes: name -mandatory, Gender(enum), dateOfBirth))),
    // write programs for the following:
    //determine all the Movies of a certain type;
    //count all Actors from a Gender;
    //retrieve all Actors;
    //calculate the sum of all imdb rates;
    //determine all the movie types
    //determine all Actors that are born in 21st century;
    //determine the best rating;
    public static void main(String[] args) {
        Actor actors = new Actor("nam", Gender.MALE, LocalDate.of(1990, 2, 12));
        Actor actors1 = new Actor("khj", Gender.FEMALE, LocalDate.of(1997, 9, 22));
        Actor a = new Actor("hgga", Gender.MALE, LocalDate.of(2020, 4, 20));
        List<Actor> actors22 = List.of(actors1, a);
        List<Actor> actors33 = List.of(actors, actors1);
        Movie m = new Movie("jkd", 1987, 9.5, MovieType.ACTION, actors22);
        Movie mm = new Movie("xx", 1990, 10.0, MovieType.DRAMA, actors33);
        Movie mmm = new Movie("vv", 2000, 7.0, MovieType.ROMANTIC, actors22);
        Set<Movie> movies = Set.of(m, mm, mmm);
        System.out.println(certainMovieTypes(movies, MovieType.DRAMA));
        System.out.println(numberOfActorsByGender(movies, Gender.FEMALE));
        System.out.println(retrieveAllActors(movies));
        System.out.println(sumOfImdb(movies));
        System.out.println(movieTypesList(movies));
        System.out.println(bornIn21St(movies));
        System.out.println(bestRating(movies).getAsDouble());
        System.out.println(moviesWithBestRating(movies));
    }

    private static List<Movie> certainMovieTypes(Set<Movie> movies, MovieType movieType) {
        return movies.stream().filter(m -> m.movieType() == movieType).toList();
    }

    private static long numberOfActorsByGender(Set<Movie> movies, Gender gender) {
        //  return movies.stream().flatMap(m -> m.actors().stream()).filter(a -> a.gender() == gender).count();

        return movies.stream().map(Movie::actors).flatMap(List::stream).filter(a -> a.gender() == gender).count();
    }

    private static Set<Actor> retrieveAllActors(Set<Movie> movies) {
        return movies.stream().flatMap(m -> m.actors().stream()).collect(Collectors.toSet());
    }

    //calculate the sum of all imdb rates
    private static double sumOfImdb(Set<Movie> movies) {
        return movies.stream().mapToDouble(m -> m.imdbRate()).sum();
    }

    //determine all the movie types
    private static Set<MovieType> movieTypesList(Set<Movie> movies) {
        return movies.stream().map(Movie::movieType).collect(Collectors.toSet());
    }

    //determine all Actors that are born in 21st century;
    //determine the best rating;
    private static Set<Actor> bornIn21St(Set<Movie> movies) {
        return movies.stream().flatMap(m -> m.actors().stream()).filter(a -> a.dateOfBirth().getYear() > 2000).collect(Collectors.toSet());
    }

    private static OptionalDouble bestRating(Set<Movie> movies) {
        return movies.stream().mapToDouble(m -> m.imdbRate()).max();
    }

    private static List<Movie> moviesWithBestRating(Set<Movie> movies) {
        Map<Double, List<Movie>> nou = movies.stream().collect(Collectors.groupingBy(Movie::imdbRate));
        Map.Entry<Double, List<Movie>> max  = nou.entrySet().stream().max(Comparator.comparing(Map.Entry::getKey)).orElseThrow();
        return max.getValue();
//       nou.entrySet().stream().max(Comparator.comparing())
    }


}
