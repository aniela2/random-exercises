package exercise3;

import java.math.BigDecimal;

public record MinMax(Number min, Number max) {

    public MinMax(Number min, Number max) {
        if (BigDecimal.valueOf(min.doubleValue()).compareTo(BigDecimal.valueOf(max.doubleValue())) > 1) {
            throw new IllegalArgumentException();
        }
        this.min = min;
        this.max = max;

    }

}
