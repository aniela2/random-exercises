package exercise3;

import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.*;
import java.util.stream.Collectors;

public class Exercisess {

    //Write a Java program to find the largest and smallest element from a List of natural numbers

    public static void main(String[] args) {
        List<Integer> naturalNumbers = List.of(1, 2, 3, 4, 5);
        System.out.println(extremeList(naturalNumbers));
        System.out.println(areGreater(1, 2, 1));
        Student one = new Student("name1", 1, List.of(10, 9, 7));
        Student two = new Student("2", 2, List.of(8, 9, 7));
        Student tree = new Student("3", 3, List.of(8, 8, 8));
        Student four = new Student("4", 4, List.of(1, 2, 3));
        Student five = new Student("2", 2, List.of(8, 9, 7));
        List<Student> sil = List.of(one, two, tree, four, five);
        System.out.println(extremeScores(sil));
        GovernmentForm g = GovernmentForm.MONARCHY;
        System.out.println("..........");
        System.out.println(g);
        GovernmentForm c = GovernmentForm.valueOf("MONARCHY");
        System.out.println(c);
        GovernmentForm[] ar = GovernmentForm.values();
        System.out.println(Arrays.toString(ar));
    }

    //Write a Java program which accepts Students (name, id, and marks)
    //and displays the highest score and the lowest score.
    private static MinMax extremeScores(List<Student> students) {
        Map<Integer, Double> map = students.stream()
                .collect(Collectors.toMap(Student::id,
                        s -> s.marks().stream()
                                .mapToInt(m -> m)
                                .average()
                                .orElse(0.0), (u, v) -> v)
                );

        return new MinMax(Collections.min(map.values()), Collections.max(map.values()));
    }

    private static MinMax extremeList(List<Integer> numbers) {
        IntSummaryStatistics i = numbers.stream().mapToInt(n -> n).summaryStatistics();
        return new MinMax(i.getMin(), i.getMax());
    }
//Write a program to determine if a given String can be found in a List at a given index.

    public static boolean areFoundInList(List<String> list, String given, Integer index) {
        return list.get(index).equals(given);
    }


    //Write a Java program which accepts three integers and check whether
    // the sum of the first two given integers is greater than the third one.
    public static boolean areGreater(Integer one, Integer two, Integer tree) {
        return one + two > tree;
    }

}
