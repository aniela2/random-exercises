package exercise3;

import java.util.Objects;

public class Ex {
    public static void main(String[] args) {
        System.out.println(myMethod(null));
    }

    private static boolean myMethod(final String input) {
        return "A".equals(input);
    }
}
