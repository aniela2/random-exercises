package exercise3;

import com.sun.source.tree.UsesTree;
import ro.aniela.Car;
import ro.aniela.exercise34.Plane;

import javax.swing.plaf.IconUIResource;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    int a = 2;

    //Write a Planets enum that has a field called yearLength.
    //The value of each yearLength should be the number of earth days of a year on the given planet:
    //Mercury: 88
    //Venus: 225
    //Earth: 365
    //Mars: 687
    //Jupiter: 4333
    //Saturn: 10759
    //Uranus: 30687
    //Neptune: 60200
    //Display into the console only the planets that have a yearLength bigger than 365


    public static void main(String[] args) {

        List<String> strings= List.of("unu", "doi");
        System.out.println(Exercisess.areFoundInList(strings, "doi", 1));

        yearBiggerThan365();
        Country c1 = new Country(10000, "Anglia", GovernmentForm.MONARCHY);
        Country c2 = new Country(2000, "Bulgaria", GovernmentForm.REPUBLIC);
        Country c3 = new Country(1500, "Austria", GovernmentForm.COMUNIST);
        Set<Country> countries = Set.of(c1, c2, c3);
        System.out.println(totalPopulation(countries));
        System.out.println(averagePopulation(countries));
        System.out.println(startsWithAOrB(countries));
//        Italian i = new Italian();
//        i.printlnHello("bon giorno!");
        switchEx(LanguagesGenerator.EN);
        Carnivores car = new Carnivores();
        Erbivores erb = new Erbivores();
        Carnivores carn = new Carnivores();
        Erbivores erbi = new Erbivores();
        List<Animal> animalList = List.of(carn, car, erb, erbi);
        System.out.println(onlyOne(animalList));
        List<String> list = List.of("gh", "are", "mere", "jg");
        List<String> lists = List.of("mama", "are", "mere");
        System.out.println(areEquals(list, lists, 1));
        Map<Integer, String> map = Map.of(1, "nn", 2, "hv");
        Map<Integer, List<String>> mapp = Map.of(1, lists, 2, list, 3, lists);
        System.out.println(valueOfMapUppercase(map));
        System.out.println(allStringsPrefixed(mapp, "g"));
        String sx = "I have a string";
        System.out.println(builldaMap(sx));
        Tupple y = new Tupple("v", "hf");
        User u = new User("vg", y, 1, "n");
        User yy = new User("", y, 2, "");
        Map<Integer, User> mapy = Map.of(1, u, 2, yy);
        System.out.println(averageOfAge(mapy));
        System.out.println(nameOfWeekDay(5));
        String[] aargs = {"hh", "hs", "jghh"};
        System.out.println(buildAString(aargs));
        int a=4;
        changeValues(a);
        System.out.println(a);
        //Chips c = new Chips();
       // c.changeValue(3);
        //  System.out.println(c.getA());

    }

    private static void changeValues(int b){
        b=5;}
    //Having an array of Strings, build a String from last two elements.
    private static String buildAString(String[] args) {
        return Arrays.stream(args).skip(args.length - 2).collect(Collectors.joining());
    }


    //Given a number from 1 to 7, write a Java program that displays the name of the weekday.
    // If the number is not in range, print “This is not a day”. Use an enum for this exercise.
    private static String nameOfWeekDay(int numberOfDay) {
        return Arrays.stream(NameOfTheDay.values())
                .filter(n -> numberOfDay == n.getNumberAsociate())
                .findFirst()
                .map(m -> m.name())
                .orElseGet(() -> "ThisIsNotADay");
    }


    //Class User(name - Tuple(attributes: first, second), age, city).
    // Having a Map of Integer and User, determine the age average.
    private static double averageOfAge(Map<Integer, User> map) {
        return map.values().stream().mapToInt(u -> u.age()).average().orElseGet(() -> 0.0);
    }

    //Having a phrase, build a Map of natural numbers and String for each word.
//E.g.: phrase= “I have a motorbike”
//result = 1, “I”;
//	      2, “have”....
    private static Map<Integer, String> builldaMap(String phrase) {
        String[] s = phrase.split(" ");
        // Map<Integer, String> mao = new HashMap();
        // for (int i = 0; i < s.length; i++) {
        //      mao.put(i, s[i]);
        // }
        return IntStream.range(0, s.length).boxed().collect(Collectors.toMap(Function.identity(), i -> s[i]));

    }


    //Having a List of Strings, write a Map of natural numbers and String.
    // The keys should be incremented by 1(one) and the values are the elements from the given List
    private static Map<Integer, String> writeAMap(List<String> list) {
        Map<Integer, String> map = new HashMap();

        for (int i = 0; i <= list.size(); i++) {
            map.put(i, list.get(i));
        }

        return IntStream.rangeClosed(0, list.size() - 1)
                .boxed()
                .collect(Collectors.toMap(Function.identity(), list::get));
    }

    //Having a Map of Integer and List of Strings, determine all the Strings prefixed with a given String.
    private static List<String> allStringsPrefixed(Map<Integer, List<String>> map, String given) {
        return map.values().stream().flatMap(List::stream).map(given::concat).toList();
        // return map.values().stream().flatMap(s -> s.stream()).filter(s -> s.startsWith(given)).toList();
    }

    //Display in uppercase the values of a Map of natural numbers and String
    private static List<String> valueOfMapUppercase(Map<Integer, String> map) {
        return map.values().stream().map(String::toUpperCase).toList();
    }

    // Having two List of String, write a program to determine if the elements at a given index are equal.
    private static boolean areEquals(List<String> s, List<String> ss, int index) {
        if (index > s.size() - 1 || index > ss.size() - 1) {
            return false;
        } else {
            return s.get(index).equals(ss.get(index));
        }
    }

    private static List<Animal> onlyOne(List<Animal> animalList) {
        return animalList.stream().filter(a -> a instanceof Carnivores).toList();
    }

    private static void switchEx(LanguagesGenerator lg) {
        switch (lg) {
            case EN -> lg.getLanguageSupplier().get().printlnHello("hello");
            case IT -> lg.getLanguageSupplier().get().printlnHello("ciao");
            case RO -> lg.getLanguageSupplier().get().printlnHello("buna");
        }
    }

    private static void yearBiggerThan365() {

        Arrays.stream(Planet.values()).filter(Main::condition).forEach(System.out::println);
    }

    private static boolean condition(Planet p) {
        return p.getYearLength() > 365;
    }


    // Add 3 objects instantiated from Country class to a Set.
    //Write methods to:
    //determine the total population of all countries;
    //determine the population’s average;
    //display into the console only countries whose name starts with “A” or “B” and are not monarchies.

    private static int totalPopulation(Set<Country> countries) {
        return countries.stream().mapToInt(c -> c.population()).sum();
    }

    private static double averagePopulation(Set<Country> countries) {

        return totalPopulation(countries) / (double) countries.size();
    }

    private static List<Country> startsWithAOrB(Set<Country> countries) {
        Predicate<Country> c12 = c -> c.name().startsWith("A");
        Predicate<Country> c13 = c -> c.name().startsWith("B");
        Predicate<Country> c14 = c -> c.governmentForm() == GovernmentForm.MONARCHY;
        return countries.stream()
                .filter(c12.or(c13).and(c14.negate()))
                //  .filter(c -> (c.name().startsWith("A") || c.name().startsWith("B")) && c.governmentForm() != GovernmentForm.MONARCHY)
                .toList();

    }
}
