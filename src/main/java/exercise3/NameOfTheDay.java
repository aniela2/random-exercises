package exercise3;

public enum NameOfTheDay {
    MONDAY(1), TUESDAY(2), WEDNESDAY(3), THURSDAY(4), FRIDAY(5);
    private final int numberAsociate;

    NameOfTheDay(int numberAsociate) {
        this.numberAsociate = numberAsociate;
    }
    public int getNumberAsociate(){
        return numberAsociate;
    }
}
