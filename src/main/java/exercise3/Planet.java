package exercise3;

public enum Planet {

    MERCURY(88),

    VENUS(225),

    EARTH(365),

    MARS(687),

    JUPITER(4333),

    SATURN(10759),

    URANUS(10759),

    NEPTUNE(60200);

    private final int yearLength;

    private Planet(int yearLength) {
        this.yearLength = yearLength;
    }

    public int getYearLength(){
        return yearLength;
    }
}
