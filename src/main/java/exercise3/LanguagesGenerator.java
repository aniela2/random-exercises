package exercise3;

import java.util.function.Supplier;

public enum LanguagesGenerator {
    IT(Italian::new), RO(Romanian::new), EN(English::new);
    private final Supplier<Language> languageSupplier;

    LanguagesGenerator(Supplier<Language> languageSupplier) {
        this.languageSupplier = languageSupplier;
    }

    public Supplier<Language> getLanguageSupplier() {
        return languageSupplier;
    }
}
