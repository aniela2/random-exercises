package exercise3;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Examplee {

    public static void main(String[] args) {
        List<Integer> i = new ArrayList<>();
        i.add(1);
        i.add(2);
        i.add(6);
        i.add(3);
        List<Integer> ix = List.of(1, 4);
        printReverse(i);
        // printFirst(i);
        // printReverse(ix);
        String s = "A b a A";
        System.out.println(checkMatching(s));
        System.out.println("lalalala");
        System.out.println(checkIfUnique("Ana are mere"));
        System.out.println(buildAString("ana", "hh"));
    }

    //Build a new List containing all the elements from two given Lists.
    private static List<Integer> integerList(List<Integer> i, List<Integer> b) {
//        List<Integer> xy = new ArrayList<>(i);
//        xy.addAll(b);
//        return xy;
        //Collections.copy(i, b);
        return Stream.concat(i.stream(), b.stream()).toList();
    }


    //Write a Java program to create a new string taking the first
    // character from the first string  and the last characters from the second string.
    // If the length of either string is 0 use "#" for missing character.

    private static String buildAString(String s, String x) {
        String y = (Objects.isNull(s) || s.length() == 0) ? "#" : s.substring(0, 1);
        String v = (Objects.isNull(x) || x.length() == 0) ? "#" : x.substring(x.length() - 1);
        return y.concat(v);
    }


    //Write a Java program to check if a given string has all unique characters
    public static boolean checkIfUnique(String s) {
        return Arrays.stream(s.split("")).collect(Collectors.toSet()).size() == s.length();
    }


    //Write a Java program to check if the first two characters are matching the last two characters of a given string.

    private static boolean checkMatching(String string) {
//        String[] args = string.split("");
//        if (args[0].equals(args[args.length - 1])) {
//            if (args[1].equals(args[args.length - 2])) {
//                return true;
//      //     }
//        }
        // return false;
        return string.substring(0, 1).equals(string.substring(string.length() - 2, string.length() - 1));

    }

    //Write a Java method to check if every digit of a given integer is even. Return true if every digit is odd otherwise false
    private static boolean isDigitEven(Integer in) {
//        String[] a = in.toString().split("");
//        for (String i : a) {
//            if (Integer.valueOf(i) % 2 == 1) {
//                return false;
//            }
        while (in > 0) {
            if (in % 2 == 1) {
                return false;
            }
            in = in / 10;
        }
        return true;
    }


    private static void printReverse(List<Integer> x) {
        Collections.reverse(x);

        System.out.println(x);
    }


    private static void printFirst(List<Integer> x) {
        System.out.println(x.get(0));
    }
}
