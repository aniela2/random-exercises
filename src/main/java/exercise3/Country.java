package exercise3;

public record Country(Integer population, String name, GovernmentForm governmentForm) {

}
