package exercise3;

import java.util.List;

public record Student(String name, Integer id, List<Integer> marks) {
    public Student(String name, Integer id, List<Integer> marks) {
        this.name = name;
        this.id = id;
        this.marks = List.copyOf(marks);
    }

}
