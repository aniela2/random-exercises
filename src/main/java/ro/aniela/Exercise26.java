package ro.aniela;

public class Exercise26 {

   // Write a program that returns only the numbers within a String
    public static void main(String[]args){
        System.out.println(withinAString("12hjd245"));
    }

    public static String withinAString(String l){
        String s="";
       char[]n= l.toCharArray();
       for(char c:n){
          if(Character.isDigit(c)){
              s+=c;
          };
       }
       return s;
    }

}
