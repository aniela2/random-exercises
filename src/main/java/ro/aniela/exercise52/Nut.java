package ro.aniela.exercise52;

public class Nut extends Ingredient {
    private Category category;

    public Nut(Category category, String name, int quantity) {
        super(name, quantity);
        this.category = category;
    }

    public Category getCategory() {
        return category;
    }
}
