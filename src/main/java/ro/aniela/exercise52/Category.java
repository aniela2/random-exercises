package ro.aniela.exercise52;

import java.util.List;

public enum Category {
    ONE(List.of(Type.SMOOTHIE)),
    TWO(List.of(Type.BANNED)),
    THREE(List.of(Type.EDIBLE, Type.COCKTAIL));
    private List<Type> madeFor;

    private Category(List<Type> madeFor) {
        this.madeFor = madeFor;
    }

    public List<Type> getMadeFor() {
        return madeFor;
    }

}

