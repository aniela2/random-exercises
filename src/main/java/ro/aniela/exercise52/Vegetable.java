package ro.aniela.exercise52;

public class Vegetable extends Ingredient {
    private Boolean isRipe;

    public Vegetable(Boolean isRipe, String name, int quantity){
        super(name, quantity);
        this.isRipe=isRipe;
    }
    public Boolean getIsRipe(){
        return isRipe;
    }
}
