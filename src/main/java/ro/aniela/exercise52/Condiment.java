package ro.aniela.exercise52;

public class Condiment extends Ingredient{
    private Category category;

    public Condiment(Category category, String name, int quantity){
        super(name, quantity);
        this.category=category;
    }
    public Category getCategory(){
        return category;
    }
}
