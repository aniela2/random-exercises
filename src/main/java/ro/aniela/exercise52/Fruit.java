package ro.aniela.exercise52;
public class Fruit extends Ingredient{
    private Boolean isRipe;

    public Fruit(Boolean isRipe, String name, int quantity){
        super(name, quantity);
        this.isRipe=isRipe;
    }
    public Boolean getIsRipe(){
        return isRipe;
    }

}
