package ro.aniela.exercise52;

public enum Type {
    SMOOTHIE, EDIBLE, COCKTAIL, BANNED;
}
