package ro.aniela;

import java.time.LocalDate;

public class Exercise6 {
    public static void main(String[] args) {
        LocalDate from = LocalDate.of(2022, 5, 4);
        LocalDate to = from.plusYears(1);
        Subscription s = new Subscription("jdf", from, to, SubscriptionType.GOLD);

        System.out.println(renew(s));
        System.out.println(s);
    }

    private static Subscription renew(Subscription subscription) {

        if (subscription.getTo().minusDays(60).isAfter(LocalDate.now())) {
            throw new RuntimeException();
        }
        //  LocalDate rew = ;

        return  new Subscription(subscription.getUserld(), subscription.getFrom(), subscription.getTo().plusYears(1), subscription.getType());
    }
}
