package ro.aniela;

import java.util.ArrayList;
import java.util.List;

public class Exercise49 {

    public static void main(String[]args){
        List<Integer>d=List.of(1,2,3);
        List<Integer>m=List.of(5,6,7);
        System.out.println(buildAList(d,m));
    }

    public static List<Integer> buildAList(List<Integer> one, List<Integer> two){
     List<Integer> s=new ArrayList<>();
     s.addAll(one);
     s.addAll(two);
     return s;
    }

}
