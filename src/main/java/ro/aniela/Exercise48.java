package ro.aniela;

import java.util.List;

public class Exercise48 {
    public static void main(String[]args){

        List<Integer>list= List.of(1,2,3,4,5);
        Statistic s= new Statistic(list);
        s.count();
        s.average();
        s.max();
        System.out.println(s.min());
        System.out.println(s.sum());
    }

}
