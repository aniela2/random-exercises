package ro.aniela;

import java.time.LocalDate;
import java.util.Objects;

public class Chocolate {
    private double weight;
    private String description;
    private LocalDate productionDate;
    private String madeBy;
    private ChocolateType type;
    private int calories;


    public Chocolate(double weight, String description, String madeBy, LocalDate productionDate, ChocolateType type, int calories) {
        if (description.length() > 100 || Objects.isNull(type) || Objects.isNull(productionDate) || calories > 600) {
            throw new RuntimeException();
        }
        this.weight = weight;
        this.description = description;
        this.madeBy=madeBy;
        this.productionDate = productionDate;
        this.type = type;
        this.calories = calories;
    }

    public double getWeight() {
        return weight;
    }

    public String getDescription() {
        return description;
    }
    public String getMadeBy(){
        return madeBy;
    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    public ChocolateType getType() {
        return type;
    }

    public int getCalories() {
        return calories;
    }

    @Override
    public String toString() {
        return "Chocolate{" +
                "weight=" + weight +
                ", description='" + description + '\'' +
                ", productionDate=" + productionDate +
                ", madeBy='" + madeBy + '\'' +
                ", type=" + type +
                ", calories=" + calories +
                '}';
    }
}
