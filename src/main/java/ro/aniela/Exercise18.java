package ro.aniela;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Exercise18 {
    //Write a Java program to reverse elements in an array list.

    public static void main(String[]args){
        List<Integer> list=new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        System.out.println(reverse(list));
    }
    private static List<Integer> reverse(List<Integer> list){
        list.sort(Comparator.reverseOrder());
        return list;
    }
}
