package ro.aniela;

public class Exercise42 {
    public static void main(String[] args) {
        System.out.print(naturalNumber(1,5));


    }

    private static StringBuilder naturalNumber(int first, int last){
        StringBuilder numbers= new StringBuilder();
        for(int i=first; i<=last; i++){
            numbers.append(i).append(" ");
        }
        return numbers;
    }
}
