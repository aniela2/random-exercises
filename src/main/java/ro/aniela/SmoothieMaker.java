package ro.aniela;

import ro.aniela.exercise52.*;

import javax.management.ValueExp;
import java.util.List;

public class SmoothieMaker {
    private List<Ingredient> ingredients;

    public SmoothieMaker(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public static Smoothie makeSmoothie(List<Ingredient> ingredients) {
        for (Ingredient i : ingredients) {
            if (i instanceof Fruit) {
                if (!(((Fruit) i).getIsRipe())) {
                    throw new IllegalIngredientException();
                }
            }
            if (i instanceof Vegetable) {
                if (!((Vegetable) i).getIsRipe()) {
                    throw new IllegalIngredientException();
                }
            }
            if (i instanceof Nut) {
                if (!(((Nut) i).getCategory().getMadeFor().contains(Type.SMOOTHIE))) {
                    throw new IllegalIngredientException();
                }

            }
            if (i instanceof Condiment) {
                if (!((Condiment) i).getCategory().getMadeFor().contains(Type.SMOOTHIE)) {
                    throw new IllegalIngredientException();
                }
            }
        }
        return new Smoothie();

    }
}
