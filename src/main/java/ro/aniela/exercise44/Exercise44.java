package ro.aniela.exercise44;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Exercise44 {
    public static void main(String[] args) {
        Employee e= new Employee(1000,"jnjh");
        System.out.println(salaryPerDayOfEmployee(e,2023));
    }

    private static Map<Integer, Double> salaryPerDayOfEmployee(Employee employs, int year) {
        Map<Integer, Double> salaryPerMonth = new HashMap<>();

        for (int month = 1; month <= 12; month++) {
            double sPM = 0d;
            int daysOfHolidays = 0;
            int lenghtMonth = LocalDate.of(year, month, 1).lengthOfMonth();
            for (int day = 1; day <= lenghtMonth; day++) {
                LocalDate ld = LocalDate.of(year, month, day);
                if (!(ld.getDayOfWeek() == DayOfWeek.SUNDAY
                        || ld.getDayOfWeek() == DayOfWeek.SATURDAY)
                        || Constants.HOLIDAYS.contains(ld)) {
                    if ((ld.getMonth() == Month.JULY || ld.getMonth() == Month.DECEMBER) && daysOfHolidays < 10) {
                        daysOfHolidays++;
                        sPM += 0.8 * employs.getSalaryPerDay();
                    } else {
                        sPM += employs.getSalaryPerDay();
                    }
                }
            }
            salaryPerMonth.put(month, sPM);
        }
        return salaryPerMonth;
    }
}
