package ro.aniela.exercise44;

public class Employee {
    private double salaryPerDay;
    private String name;

    public Employee(double salaryPerDay, String name) {
        this.salaryPerDay = salaryPerDay;
        this.name = name;
    }

    public double getSalaryPerDay() {
        return salaryPerDay;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return "Employee are : salary per day " + salaryPerDay + ", name" + name;
    }
}
