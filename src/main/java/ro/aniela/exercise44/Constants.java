package ro.aniela.exercise44;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

public final class Constants {
    public static final List<LocalDate> HOLIDAYS = List.of(LocalDate.of(2023, 2, 12),
            LocalDate.of(2023, 5, 1),
            LocalDate.of(2023, 5, 19));

    private Constants() {
        throw new AssertionError();
    }
}
