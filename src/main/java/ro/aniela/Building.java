package ro.aniela;

public class Building implements Comparable<Building> {
    private int numberOfFloors;
    private String name;

    public Building(int numberOfFloors, String name) {
        this.numberOfFloors = numberOfFloors;
        this.name = name;
    }

    public int getNumberOfFloors() {
        return numberOfFloors;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Building{" +
                "numberOfFloors=" + numberOfFloors +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int compareTo(Building o) {
        if (this.numberOfFloors == o.numberOfFloors) {
            return this.getName().compareTo(o.name);
        }
        return Integer.compare(this.numberOfFloors, o.numberOfFloors);
    }
}
