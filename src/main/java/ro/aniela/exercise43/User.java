package ro.aniela.exercise43;

public class User {
    private String name;
    private int id;
    private Role roles;

    public User(String name, int id, Role roles) {
        this.name = name;
        this.id = id;
        this.roles = roles;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public Role getRoles() {
        return roles;
    }
}
