package ro.aniela.exercise43;

public enum Role {
    ADMIN, GUEST, READ_WRITE;

}
