package ro.aniela.exercise43;

import javax.print.Doc;
import java.security.PublicKey;

public class Exercise43 {
    public static void main(String[] args) {
        User u = new User("mjsfj", 123, Role.GUEST);
        Document d = new Document("jhv", "Oni");
        System.out.println(changeDocumentNAme(u, d));
        contentOfDocument(u, d);
        System.out.println(eraseContent(u, d));
        System.out.println(deleteDocument(u, d));
    }

    private static boolean validateInput(final String input) {
        input.split("");
        return true;

    }

    private static Document changeDocumentNAme(User user, Document document) {
        String doc = document.getName();
        if (user.getRoles() == Role.ADMIN || user.getRoles() == Role.READ_WRITE) {
            doc = doc.concat("v1");
        } else {
            throw new RuntimeException("You do not have the grants to do this operation");
        }
        return new Document(document.getContent(), doc);
    }

    private static void contentOfDocument(User u, Document d) {
        System.out.println(d.getName().concat(" ").concat(d.getContent()));
    }

    private static Document eraseContent(User u, Document d) {
        if (u.getRoles() == Role.ADMIN) {
            return new Document("", "ghvh");
        } else {
            throw new RuntimeException("You do not have the grants to do this operation");
        }
    }

    private static Document deleteDocument(User u, Document d) {
        if(u.getRoles()==Role.ADMIN){
            return new Document(null, null);
        }else {
            throw new RuntimeException("You do not have the grants to do this operation");
        }
    }
}
