package ro.aniela.exercise43;

public class Document {
    private String content;
    private String name;
    public Document(String content, String name){
        this.content=content;
        this.name=name;
    }

    public String getContent() {
        return content;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Document{" +
                "content='" + content + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
