package ro.aniela;

public class Exercise31 {
    public static void main(String[]args){

        System.out.println(sumAndAverageOfNumber(1,2,3,5,7));
    }

    private static Pair<Integer, Double> sumAndAverageOfNumber(Integer... numbers){
        Integer sum=0;

        for(Integer p : numbers) {
            sum+=p;

        }
       double average=(double)sum/numbers.length;
        return new Pair<>(average, sum);
    }
}
