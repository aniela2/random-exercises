package ro.aniela.exercise34;

public abstract class Vehicle {
    private int numberOfSeats;

    public Vehicle(int numberOfSeats){
        this.numberOfSeats=numberOfSeats;
    }

    public int getNumberOfSeats(){
        return numberOfSeats;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "numberOfSeats=" + numberOfSeats +
                '}';
    }
}

