package ro.aniela.exercise34;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Exercise34 {

    public static void main(String[] args) {
        Vehicle v = new Car(2);
        Vehicle c = new Car(4);
        Vehicle plane = new Plane(100);
        Vehicle bi = new Bike(2);
        Vehicle b = new Bus(4);
        Vehicle m = new MotorBike(1);
        Map<Vehicle, Integer> map = Map.of(v, v.getNumberOfSeats(),
                c, c.getNumberOfSeats(),
                plane, plane.getNumberOfSeats(),
                bi, bi.getNumberOfSeats(),
                b, b.getNumberOfSeats(),
                m, m.getNumberOfSeats());
        System.out.println(map);
    }


}
