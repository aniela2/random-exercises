package ro.aniela.exercise55;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

public class Person {
    private int age;
    private Pattern pattern;
    private List<LocalDateTime> meals;

    public Person(int age, Pattern pattern, List<LocalDateTime> meals) {
        this.age = age;
        this.pattern = pattern;
        this.meals = meals;
    }

    public int getAge() {
        return age;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public List<LocalDateTime> getMeals() {
        return meals;
    }
}
