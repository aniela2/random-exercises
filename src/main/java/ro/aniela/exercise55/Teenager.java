package ro.aniela.exercise55;

import java.time.LocalDateTime;
import java.util.List;

public class Teenager extends Person{
    public Teenager(int age, Pattern pattern, List<LocalDateTime> meals){
        super(age, pattern, meals);
        if(meals.size()<2){
            throw new IllegalArgumentException();
        }
        if(pattern != Pattern.TWELVE){
            throw new IllegalArgumentException();
        }
    }
    }
