package ro.aniela.exercise55;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Exercise55 {
    public static void main(String[] args) {
        LocalDateTime ld= LocalDateTime.of(LocalDate.now(), LocalTime.of(8, 00));
        LocalDateTime dj= LocalDateTime.of(LocalDate.now(), LocalTime.of(5,00));
        LocalDateTime hj=LocalDateTime.of(LocalDate.now(), LocalTime.of(5, 00));
        Person one= PersonFactory.create(15, Pattern.TWELVE, List.of(ld, dj, hj));
        Person two= PersonFactory.create(22, Pattern.BLACK, List.of(ld));
        Person three= PersonFactory.create(80, Pattern.OMAD, List.of(dj, hj,hj));
        List<Person> personList= List.of(one, two, three);
        System.out.println(theFirstMeal(personList));
    }

    private static List<LocalDateTime> theFirstMeal(List<Person> persons) {
       List<LocalDateTime> ldt= new ArrayList<>();
        for (Person p : persons) {
            if (p instanceof Child) {
                ldt.add(LocalDateTime.of(LocalDate.now().plusDays(1), LocalTime.of(8, 00)));
            }
            if (p instanceof Teenager || p instanceof Adult || p instanceof Elder) {
                ldt.add(p.getMeals().get(p.getMeals().size() - 1).plusHours(p.getPattern().getFastTime()));
            }
        }
        return ldt;
    }

}
