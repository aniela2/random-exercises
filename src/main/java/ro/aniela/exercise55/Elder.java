package ro.aniela.exercise55;

import java.time.LocalDateTime;
import java.util.List;

public class Elder extends Person {
    public Elder(int age, Pattern pattern, List<LocalDateTime> meals) {
        super(age, pattern, meals);
        if(meals.size()<3){
            throw new IllegalArgumentException();
        }
    }
}
