package ro.aniela.exercise55;

public enum Pattern {

    OMAD(24),
    TWENTY(20),
    SIXTEEN(16),
    TWELVE(12),
    BLACK(0);
    private int fastTime;

    private Pattern(int fastTime) {
        this.fastTime = fastTime;
    }

    public int getFastTime() {
        return fastTime;
    }
}
