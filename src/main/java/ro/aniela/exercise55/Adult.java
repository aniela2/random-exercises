package ro.aniela.exercise55;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

public class Adult extends Person{
    public Adult(int age, Pattern pattern, List<LocalDateTime> meals){
        super(age, pattern, meals);
    }
}
