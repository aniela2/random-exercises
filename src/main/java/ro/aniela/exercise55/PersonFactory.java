package ro.aniela.exercise55;

import java.time.LocalDateTime;
import java.util.List;

public class PersonFactory {
    public static Person create(int age, Pattern pattern, List<LocalDateTime>meals) {
        if (age < 14) {
            return new Child(age, pattern, meals);
        }
        if (age >= 14 && age < 18) {
            return new Teenager(age, pattern, meals);
        }
        if (age >= 18 && age < 60) {
            return new Adult(age, pattern, meals);
        }
        if (age >= 60) {
            return new Elder(age, pattern, meals);
        }
        throw new IllegalArgumentException();
    }
}
