package ro.aniela.exercise55;

import java.time.LocalDateTime;
import java.util.List;

public class Child  extends Person{
    public Child(int age, Pattern pattern, List<LocalDateTime> meals){
        super (age, pattern, meals);
    }
}
