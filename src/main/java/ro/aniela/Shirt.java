package ro.aniela;

public class Shirt {
    private int size;
    private String color;
    private String gender;

    public Shirt(int size, String color, String gender) {
        this.size = size;
        this.color = color;
        this.gender = gender;
    }

    public int getSize() {
        return size;
    }

    public String getColor() {
        return color;
    }

    public String getGender() {
        return gender;
    }
@Override
    public String toString() {
        return " Shirt are size " + size + "color" + color + "gender" + gender;
    }
}


