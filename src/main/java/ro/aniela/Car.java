package ro.aniela;

public class Car implements Comparable<Car> {

    private String name;
    private String brand;
    private int power;

    public Car(String name, String brand, int power) {
        this.name = name;
        this.brand = brand;
        this.power = power;
    }

    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public int getPower() {
        return power;
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", power=" + power +
                '}';
    }

    @Override
    public int compareTo(Car c) {
        return this.power - c.power;
    }
}
