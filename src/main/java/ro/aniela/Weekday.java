package ro.aniela;

public enum Weekday {
    MONDAY(1),
    TUESDAY(2),
    WEDNESDAY(3),
    THURSDAY(4),
    FRIDAY(5),
    SATURDAY(6),
    SUNDAY(7);
    private int numberOfTheDay;

    Weekday(int numberOfTheDay){
        this.numberOfTheDay=numberOfTheDay;
    }
    public int getNumberOfTheDay(){
        return numberOfTheDay;
    }
}
