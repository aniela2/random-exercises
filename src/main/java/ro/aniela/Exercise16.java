package ro.aniela;

import java.awt.event.MouseAdapter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Exercise16 {
    public static void main(String[] args) {
        Map<?, ?> mapp= Map.of();
        System.out.println(containsKeyValueOrNot(mapp));
    }

    private static boolean containsKeyValueOrNot(Map<?, ?> map) {
        return Objects.isNull(map) ||  map.isEmpty() ;
    }
}
