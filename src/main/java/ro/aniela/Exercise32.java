package ro.aniela;

public class Exercise32 {
    public static void main(String[] args) {
        System.out.println(concat("one", "two"));
    }

    private static String concat(String one, String two) {
        String unu = one.length() == 0 ? "#" : one.substring(0,1);
        String doi = two.length() == 0 ? "#" : two.substring(two.length() - 1);
        return unu.concat(doi);

    }
}
