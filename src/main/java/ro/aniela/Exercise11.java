package ro.aniela;

public class Exercise11 {


    //Write a Java program to find the longest word within a phrase

    public static void main(String[]args){
        System.out.println(longestWord("Ana are mere"));
    }
    private static String longestWord(String phrase){
        String maxim="";
      String[]arr=  phrase.split(" ");
      for(String s: arr){
          if(s.length()> maxim.length()){
              maxim=s;
          }
      }
      return maxim;
    }
}

