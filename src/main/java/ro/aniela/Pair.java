package ro.aniela;

public class Pair<C,V>  {

    private C first;
    private V second;
    public Pair(V second, C first){
        this.second =second;
        this.first =first;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "first=" + first +
                ", second=" + second +
                '}';
    }
}
