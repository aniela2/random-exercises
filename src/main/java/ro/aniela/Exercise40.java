package ro.aniela;

import jdk.jshell.execution.LoaderDelegate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Exercise40 {

    public static void main(String[] args) {
        Chocolate c = new Chocolate(12d, "jhgss, hfgvgd", "dfhf",
                LocalDate.of(1989, 2, 22), ChocolateType.BLACK, 123);
        Chocolate t = new Chocolate(34, "jjf", "Com", LocalDate.of(2010, 1, 22), ChocolateType.WITH_MILK, 23);
        Chocolate p = new Chocolate(15, "jhfjr", "hefer", LocalDate.of(2022, 9, 12), ChocolateType.WHITE, 124);
        List<Chocolate> theList = List.of(c, t, p);
        System.out.println(producedInThisPeriod(theList, LocalDate.of(1990, 2, 11), LocalDate.of(2023, 2, 1)));
        System.out.println(producedBy(theList, "Com"));
        System.out.println(onlytheFirstChar(theList));
        System.out.println(byType(theList));
        System.out.println(averageCalories(theList));

    }

    private static List<Chocolate> producedInThisPeriod(List<Chocolate> c, LocalDate from, LocalDate to) {
        List<Chocolate> inPeriod = new ArrayList<>();
        for (Chocolate oneByOne : c) {
            if (oneByOne.getProductionDate().isAfter(from) && oneByOne.getProductionDate().isBefore(to)) {
                inPeriod.add(oneByOne);
            }
        }
        return inPeriod;
    }

    private static List<Chocolate> producedBy(List<Chocolate> list, String company) {
        List<Chocolate> p = new ArrayList<>();
        for (Chocolate c : list) {
            if (c.getMadeBy().equals(company)) {
                p.add(c);
            }
        }
        return p;
    }

    private static List<String> onlytheFirstChar(List<Chocolate> list) {
        List<String> s = new ArrayList<>();
        for (Chocolate l : list) {
            if (l.getDescription().length() > 50) {
                s.add(l.getDescription().substring(0, 50) + "...");
            } else {
                s.add(l.getDescription());
            }
        }
        return s;
    }

    private static Map<ChocolateType, List<Chocolate>> byType(List<Chocolate> list) {
        Map<ChocolateType, List<Chocolate>> map = new HashMap<>();
        for (Chocolate chocolate : list) {
            if (map.containsKey(chocolate.getType())) {
                map.get(chocolate.getType()).add(chocolate);
            } else {
                List<Chocolate> cad = new ArrayList<>();
                cad.add(chocolate);
                map.put(chocolate.getType(), cad );
            }
        }
        return map;
    }
    private static double averageCalories(List<Chocolate>list){
        int sum=0;
        int count=0;
        for(Chocolate c: list){
          sum+=c.getCalories();
          count++;
        }
        return sum/count;
    }
}
