package ro.aniela;

import java.util.ArrayList;
import java.util.List;

public class Exercise27 {
    public static void main(String[] args) {
        Shirt shirt1 = new Shirt(36, "albastru", "male");
        Shirt shirt2 = new Shirt(38, "albastru", "male");
        Shirt shirt3 = new Shirt(40, "albastru", "female");
        List<Shirt> shirts = List.of(shirt1, shirt2, shirt3);
        System.out.println(allTheSizes(shirts, "albastru", "male"));
    }

    private static List<Integer> allTheSizes(List<Shirt> list, String color, String gender) {
        List<Integer> sisez = new ArrayList<>();
        for (Shirt s : list) {

            if (color.equals(s.getColor()) && gender.equals(s.getGender())) {
                sisez.add(s.getSize());
            }

        }
        return sisez;

    }
}
