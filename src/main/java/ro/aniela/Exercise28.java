package ro.aniela;

public class Exercise28 {

    //Write a Java method to check if every digit of a given integer is odd.
    // Return true if every digit is odd otherwise false.

    public static void main(String[] args) {
        System.out.println(allEvenNumber(151));
    }

    private static boolean allEvenNumber(int number) {
        while (number > 0) {
            int digit = number % 10;
            if (digit % 2 == 0) {
                return false;
            }
            number = number / 10;
        }
        return true;
    }


}
