package ro.aniela;

public class Exercise30 {
    public static void main(String[]args){
        System.out.println(numberAndCube(2));
    }

    private static String numberAndCube(int number ){
        return number + " si " + number*number*number;
    }
}
