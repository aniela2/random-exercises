package ro.aniela;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Exercise45 {

    public static void main(String[] args) {
        Car one = new Car("hjg", "nd", 90);
        Car two = new Car("hj", "nd", 95);
        Car three = new Car("jd", "jwhd", 100);

        List<Car> cars = List.of(one, two, three);
        List<Car> cars1=new ArrayList<>();
        cars1.add(one);
        cars1.add(two);
        cars1.add(three);
        System.out.println(sortByPowerOfCars(cars1));

    }

    private static List<Car> sortByPowerOfCars(List<Car> cars) {
        Collections.sort(cars);
        return cars;
    }
}

