package ro.aniela;

public class Exercise24 {
    public static void main(String[]args){
        System.out.println(sumOfFigures(111));

    }

   // Receiving a natural number, determine the sum of the figures(digits)

    private static int sumOfFigures(int number){

       int sum=0;
        String s=String.valueOf(number);
       String[]arg= s.split("");
        for(String i: arg){
           sum+= Integer.valueOf(i);
        }
        return sum;


    }


}
