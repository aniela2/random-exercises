package ro.aniela;

import ro.aniela.eexercise35.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Exercise35 {

    public static void main(String[] args) {
        Product p = new Footwear(12d, 2, Gender1.FEMALE);
        Product n = new Footwear(12, 7, Gender1.MALE);
        Product c = new Apparel(14, 7, Gender1.MALE);
        Product a = new Accesory(1, 6, Gender1.NONE, Category.BELT);
        List<Product> products = List.of(p, n, c, a);
        System.out.println(findByGender(products, Gender1.FEMALE));
        System.out.println(byType(products));
        System.out.println(averagePrice(products));
        System.out.println(allCategoriesOfAccesories(products));
        System.out.println(sumOfThePrices(products));

    }

    private static int findByGender(List<Product> products, Gender1 gender) {
        int count = 0;
        for (Product p : products) {
            if (p.getMadeFor() == gender) {
                count++;
            }
        }
        return count;
    }

    private static Map<Class<? extends Product>, Integer> byType(List<Product> productList) {
        Map<Class<? extends Product>, Integer> map = new HashMap<>();
        for (Product p : productList) {
            if (map.containsKey(p.getClass())) {
                map.put(p.getClass(), map.get(p.getClass()) + 1);
            } else {
                map.put(p.getClass(), 1);
            }
        }
        return map;
    }

    private static double averagePrice(List<Product> footwear) {
        double sum =0d;
        int count=0;
        for(Product p:footwear){
            if(p instanceof Footwear){
                sum+= p.getPrice();
                count++;
            }
        }
        return sum/count;
    }

    private static List<Category> allCategoriesOfAccesories(List<Product> products){
        List<Category> cat= new ArrayList<>();
        for(Product p: products){
            if(p instanceof Accesory){
                cat.add(((Accesory) p).getCategory());
            }
        }
        return cat;
    }

    //calculate the sum of prices of the first 3(three)Products from a given List .\

    private static double sumOfThePrices(List<Product>products){
        double sumPrices=0d;
        for(int i=0; i<3; i++){
            sumPrices= sumPrices+ products.get(i).getPrice();
        }
        return sumPrices;
    }

}

