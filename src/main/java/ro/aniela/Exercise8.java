package ro.aniela;

import java.util.Map;
import java.util.Objects;

public class Exercise8 {
    public static void main(String[]args){
        Map<Integer, Object> mapp=Map.of(1, "no", 2 , 2.4, 3, Continent.EUROPA);
        System.out.println(strings(mapp));

    }
    private static StringBuilder strings(Map<Integer, Object>map){
        StringBuilder sb= new StringBuilder();
        for(Object o: map.values()){
            if(o instanceof String){
                sb.append(o);
            }

        }
        return sb;
    }
}
