package ro.aniela.eexercise35;

public class Employee {
    private double salary;

    public Employee (double salary){
        this.salary=salary;
    }

    public double getSalary(){
        return salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "salary=" + salary +
                '}';
    }
}
