package ro.aniela.eexercise35;

public class Apparel extends Product{

    public Apparel(double price, double size, Gender1 madeFor){
        super(price, size, madeFor);
    }

    public String toString(){
        return "Apparel : { price " + getPrice() +", size"+ getSize() + "madeFor" + getMadeFor();

    }
}
