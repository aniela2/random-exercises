package ro.aniela.eexercise35;

public record ErrorDto(Class<? extends Exception> clazz, String message) {
}
