package ro.aniela.eexercise35;

import javax.swing.*;
import java.util.function.Consumer;
import java.util.function.Function;

public class Example {

    public static void main(String[] args) {
//        catEats(d());
        catEats(Example::duplicateEx);
        //Function<String,Integer>function= b->a(b);
       Function<String,Integer> f= Example :: a;
    }


    private static int a(String b){
        return Integer.valueOf(b);
    }

    //private static void duplicateExample(String message){
    //  System.out.println(message);
    //}
    private static void duplicateEx(String msg) {
       // return message -> System.out.println(message);
         System.out.println(msg);
        // (Object... objs)
        // (Object... objs, int i)
        // (int i, Object... objs)
    }

    private static int catEats(Consumer<String> c) {
        c.accept("cats eat");
        return 1;
    }

    private static String d() {
        return "";
    }


    private static int dogEats(String message) {
        animalEats(message);
        return 2;
    }

    private static void animalEats(String message) {
        System.out.println(message);
        System.out.println(message);
        System.out.println(message);
    }
}
