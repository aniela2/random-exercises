package ro.aniela.eexercise35;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Constants {
    public static final int LOW_ALCOHOL_THRESHOLD=5;

    public static final List<LocalDate> CELEBRATE = List.of(
            LocalDate.of(1990, 02, 9),
            LocalDate.of(1990, 2, 23),
            LocalDate.of(2002, 2, 4));

    private Constants() {
        throw new AssertionError();
    }

}