package ro.aniela.eexercise35;

public enum Category {
    BELT, JEWELLERY, UNDERWEAR;
}
