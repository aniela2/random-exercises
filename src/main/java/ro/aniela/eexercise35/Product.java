package ro.aniela.eexercise35;

public abstract class Product {
    private double price;
    private double size;
    private Gender1 madeFor;

    public Product(double price, double size, Gender1 madeFor) {
        this.price = price;
        this.size = size;
        this.madeFor = madeFor;
    }

    public double getPrice() {
        return price;
    }

    public double getSize() {
        return size;
    }

    public Gender1 getMadeFor() {
        return madeFor;
    }

    @Override
    public String toString() {
        return "Product{" +
                "price=" + price +
                ", size=" + size +
                ", madeFor=" + madeFor +
                '}';
    }
}
