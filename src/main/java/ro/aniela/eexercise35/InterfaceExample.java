package ro.aniela.eexercise35;

import java.util.function.Function;

public interface InterfaceExample {

    int methodA();

    int methodB();

    default int methodC(){
        methodE();
        //currying
        Function<Integer, Function<Double, Integer>> f = a -> b -> a + b.intValue();
        return f.apply(2).apply(3d);
    }

    static int methodD(){
        return 0;
    }

    private int methodE(){
        return 1;
    }
}
