package ro.aniela.eexercise35;

public class Footwear extends Product {

    public Footwear(double price, double size, Gender1 madeFor) {
        super(price, size, madeFor);
    }

    @Override
    public String toString() {
        return "Footwear : { price" + getPrice() + "size" + getSize() + ", madeFor" + getMadeFor() + " }";
    }
}
