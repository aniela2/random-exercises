package ro.aniela.eexercise35;

public class Accesory extends Product {
    private Category category;

    public Accesory(double price, double size, Gender1 madeFor, Category category) {
        super(price, size, madeFor);
        this.category = category;
    }

    public Category getCategory() {
        return category;
    }

    public String toString() {
        return "Accesory : price" + getPrice() + "size" + getSize() + ", madeFor" + getMadeFor() + "category" + category;
    }

}
