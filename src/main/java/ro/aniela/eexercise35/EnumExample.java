package ro.aniela.eexercise35;

import java.util.List;

public enum EnumExample {
    ROMANIA, ANGLIA, BULGARIA;

    private int returnValueOfConstants(EnumExample country) {
        switch (country) {
            case ROMANIA:
                return 1;
            case ANGLIA:
                return 2;
            case BULGARIA:
                return 3;
            default:
                return 0;
        }
    }

    private int valueOfConstants(EnumExample country) {
        return switch (country) {
            case ROMANIA -> 1;
            case ANGLIA -> 2;
            case BULGARIA -> {
                yield 3;
            }
        };
    }




    private int valueOfConstants(int i) {
        return switch (i) {
            case 2 -> 1;
            case 3 -> 2;
            case 4 -> {
                yield 3;
            }
            default -> 3;
        };
    }
}
