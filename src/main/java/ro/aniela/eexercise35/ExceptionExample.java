package ro.aniela.eexercise35;

import java.io.IOException;

public class ExceptionExample {


    public static void main(String[] args) {
        d();
    }

    private String a() throws IOException {
        //call method B
        //call method C
        //b();
//        try{
        //c();

//        } catch (IOException ioe){
//            System.out.printf("Exception");
//        }

        return "";
    }

    private String b() throws RuntimeException {
        return null;
    }

    private String c() throws IOException {
        return null;
    }

    private static void d() {
        int i = learnFinally(null);
         System.out.println(i + " i");
        System.out.println("4");
    }

    private static int learnFinally(Integer input) {

        try {
            System.out.println("1");
            return input.intValue();
        } catch (IllegalArgumentException | NullPointerException |UnknownUserType e) {
            System.out.println("2");
            ErrorDto errorDto = new ErrorDto(e.getClass(), e.getMessage());
            throw new IllegalArgumentException("222222222");

//            return 2;
        }
        finally {
            System.out.println("3");
            //return 3;
            //throw  new IllegalArgumentException("33333333");
       }
    }
}
