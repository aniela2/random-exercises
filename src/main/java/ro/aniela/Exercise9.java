package ro.aniela;

public class Exercise9 {
    public static void main(String[] args) {
        System.out.println(reverseSentences("Ana are mere"));

    }

    private static StringBuilder reverseSentences(String sentences) {
        // List<String> words= Arrays.asList(sentences.split(" "));
        // Collections.reverseOrder(words);
        //sort(words, Comparator.reverseOrder());
        String[] args = sentences.split(" ");
        StringBuilder reverseSentences = new StringBuilder();
        for (int i = args.length - 1; i >= 0; i--) {
            reverseSentences.append(args[i]).append(" ");
        }
        return reverseSentences;
    }
}


