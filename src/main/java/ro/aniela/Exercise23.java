package ro.aniela;

import java.util.List;

public class Exercise23 {
    //Given a List of Lists, write a program to determine the last character of the last item
    public static void main(String[] args) {
        List<String> st = List.of("123", "456", "oni");
        List<String> str = List.of("oni", "111", "9878");

        List<List<String>> s = List.of(st, str);
        System.out.println(lastCharacter(s));


    }

    public static String lastCharacter(List<List<String>> lists) {
        List<String> last = lists.get(lists.size() - 1);
        String s = last.get(last.size() - 1);
       //System.out.println(s.lastIndexOf("y"));
        return s.substring(s.length()-1);
    }
}
