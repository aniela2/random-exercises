package ro.aniela;

public class Exercise50 {

    public static void main(String[] args) {
        int[] integ = {1, 4, 5};
        System.out.println(sumOfIndex(integ));
    }

    public static int sumOfIndex(int[] integers) {
        int sum = 0;
        int index = 0;
        for (int i : integers) {
            sum+=i + index;
            index++;
        }
        return sum;
    }
}
