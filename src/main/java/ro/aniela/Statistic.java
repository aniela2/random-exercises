package ro.aniela;

import java.util.Collections;
import java.util.List;

public class Statistic {
    private List<Integer> list;

    public Statistic(List<Integer>list){
        this.list=list;
    }
    public List<Integer> getList(){
        return list;
    }

    public int count() {
        return list.size();
    }

    public int min(){
       return Collections.min(list);
    }
    public int max(){
        return Collections.max(list);
    }
    public int sum(){
        int sum=0;
        for(Integer i:list){
            sum+=i;
        }
        return sum;
    }
    public double average(){
       return sum()/list.size()-1;
    }

    @Override
    public String toString() {
        return "Statistic{" +
                "list=" + list +
                '}';
    }
}
