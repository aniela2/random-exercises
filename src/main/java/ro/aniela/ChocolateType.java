package ro.aniela;

public enum ChocolateType {
    BLACK, WITH_MILK, WHITE;
}
