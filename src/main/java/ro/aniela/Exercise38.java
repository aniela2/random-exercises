package ro.aniela;

import java.util.ArrayList;
import java.util.List;

public class Exercise38 {
    // Having a List of Strings with numbers, build a String from last two elements which are greater than 10.

    public static void main(String[] args) {
        List<String> num = List.of("1", "2", "3");
        System.out.println(buildAString(num, 10));
    }

    private static String buildAString(List<String> numbers, int ten) {
        String a = "";
        int stop = 0;
        for (int i = numbers.size() - 1; i > 0; i--) {
            int x = Integer.valueOf(numbers.get(i));
            if (x > ten) {
                stop++;
                a = a + numbers.get(i);
            }
            if (stop <= 2) {
                return a;
            }
        }
        return a;
    }
}
