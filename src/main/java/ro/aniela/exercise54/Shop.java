package ro.aniela.exercise54;

import java.util.List;
import java.util.PrimitiveIterator;

public class Shop {
    private List<Product> products;
    private String name;
    private String address;

    public  Shop(List<Product>products, String name, String address){

        this.products=products;
        this.name=name;
        this.address=address;
    }

    public List<Product> getProducts() {
        return products;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }
}
