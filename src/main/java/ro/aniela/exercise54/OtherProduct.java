package ro.aniela.exercise54;

import java.util.List;

public class OtherProduct extends Product {
    private String category;

    public OtherProduct(String name, String madeBy, List<String> ingredients, String category) {
        super(name, madeBy, ingredients);
        this.category = category;
    }

    public String getCategory() {
        return category;
    }
}
