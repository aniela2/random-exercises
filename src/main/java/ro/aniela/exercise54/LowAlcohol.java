package ro.aniela.exercise54;

import ro.aniela.eexercise35.Constants;

import java.util.List;

public class LowAlcohol extends Beverage {
    public LowAlcohol(String name, String madeBy, List<String> ingredients, int alcoholPercent) {
        super(name, madeBy, ingredients, alcoholPercent);
        if(getAlcoholPercent()> Constants.LOW_ALCOHOL_THRESHOLD){
            throw new IllegalArgumentException();
        }
    }

}
