package ro.aniela.exercise54;

import java.util.List;

public class HighAlcohol  extends Beverage{

    public HighAlcohol(String name, String madeBy, List<String> ingredients, int alcoholPercent){
        super(name, madeBy, ingredients, alcoholPercent);
    }
}
