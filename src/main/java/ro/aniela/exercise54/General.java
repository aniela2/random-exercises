package ro.aniela.exercise54;

import ro.aniela.eexercise35.Constants;

import java.util.List;

public class General extends Shop {

    public General(List<Product> products, String name, String address) {
        super(products, name, address);
        for (Product p : products) {
            if (p instanceof Beverage) {
                if (((Beverage) p).getAlcoholPercent() > Constants.LOW_ALCOHOL_THRESHOLD) {
                    throw new IllegalArgumentException();
                }
            }
        }
    }
}
