package ro.aniela.exercise54;

import java.util.List;

public class Beverage extends Product {
    private int alcoholPercent ;

    public Beverage(String name, String madeBy, List<String> ingredients, int alcoholPercent) {
        super(name, madeBy, ingredients);
        this.alcoholPercent = alcoholPercent;
    }

    public int getAlcoholPercent (){
        return alcoholPercent;
    }

    @Override
    public String toString() {
        return "Beverage{" +
                "alcoholPercent=" + alcoholPercent +
                '}';
    }
}
