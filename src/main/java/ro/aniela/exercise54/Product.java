package ro.aniela.exercise54;

import java.util.List;

public class Product {
    private String name;
    private String madeBy;
    private List<String> ingredients;

    public Product(String name, String madeBy, List<String> ingredients) {
        this.name = name;
        this.madeBy = madeBy;
        this.ingredients = ingredients;
    }

    public String getName() {
        return name;
    }

    public String getMadeBy() {
        return madeBy;
    }

    public List<String> getIngredients() {
        return ingredients;
    }
}
