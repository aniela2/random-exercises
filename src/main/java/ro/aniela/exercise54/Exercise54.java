package ro.aniela.exercise54;

import com.sun.source.tree.BreakTree;

import java.util.ArrayList;
import java.util.List;

public class Exercise54 {
    public static void main(String[] args) {
        User u1=new User("jj", 18);
        Product product= new Beverage("hhf", "kjj", List.of("hh"), 2);
        Shop pp=new General(List.of(product), "jhg", "jh");
        System.out.println(user(u1, pp));
    }

    private static List<Product> user(User u, Shop s) {
        List<Product> validToBy = new ArrayList<>();
        for (Product p : s.getProducts()) {
            if (p instanceof Beverage) {
                if (u.getAge() < 18) {
                    if ((((Beverage) p).getAlcoholPercent()) == 0) {
                        validToBy.add(p);
                    }
                } else {
                    validToBy.add(p);
                }
            } else {
                validToBy.add(p);
            }
        }
        return validToBy;
    }

}
