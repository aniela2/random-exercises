package ro.aniela.exercise54;

import ro.aniela.exercise52.IllegalIngredientException;

import java.util.List;

public class LiquorShop extends Shop {

    public LiquorShop(List<Product> products, String name, String address, int alcoholPercent) {
        super(products, name, address);
        for(Product p:products){
            if(!(p instanceof Beverage)){
               throw new IllegalArgumentException ();
            }
        }
    }

}
