package ro.aniela.exercise39;

import java.security.PublicKey;
import java.util.Objects;

public class Ingredient implements Comparable<Ingredient> {

    private IngredientType type;
    private String name;
    private double weight;

    public Ingredient(IngredientType type, String name, double weight) {
        if (weight < 10) {
            throw new IllegalArgumentException();
        }
        this.type = type;
        this.name = name;
        this.weight = weight;
    }

    public IngredientType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ingredient that = (Ingredient) o;
        return Double.compare(that.weight, weight) == 0 && type == that.type && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, name, weight);
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "type=" + type +
                ", name='" + name + '\'' +
                ", weight=" + weight +
                '}';
    }

    @Override
    public int compareTo(Ingredient o) {
        return Double.compare(weight, o.weight);
    }
}
