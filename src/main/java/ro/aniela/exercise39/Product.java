package ro.aniela.exercise39;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Product {
    private String name;
    private List<Ingredient> ingredients;
    private LocalDate bestBefore;

    public Product(String name, List<Ingredient> ingredients, LocalDate bestBefore){
        if(ingredients.size()>10 || (Objects.isNull(name) && name.isBlank())){
            throw new IllegalArgumentException();
        }
        this.name=name;
        this.ingredients=ingredients;
        this.bestBefore=bestBefore;
    }
    public String getName(){
        return name;
    }
    public List<Ingredient> getIngredients(){
        return ingredients;
    }
    public LocalDate getBestBefore(){
        return bestBefore;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", ingredients=" + ingredients +
                ", bestBefore=" + bestBefore +
                '}';
    }
}
