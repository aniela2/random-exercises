package ro.aniela.exercise39;

import java.time.LocalDate;
import java.util.*;

public class Exercise39 {
    public static void main(String[] args) {
        Ingredient i = new Ingredient(IngredientType.ANIMAL, "hhg", 14);
        Ingredient j = new Ingredient(IngredientType.VEGAN, "jhv", 10);
        Ingredient m = new Ingredient(IngredientType.VEGETARIAN, "jehfj", 19);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(i);
        List<Ingredient> ingred = new ArrayList<>();
        ingred.add(i);
        ingred.add(j);
        ingred.add(m);
        Product p = new Product("hbdf", ingred, LocalDate.of(2024, 3, 01));
        Product jap = new Product("jnj", ingredients, LocalDate.of(2010, 9, 01));
        Product ma = new Product("Al", ingred, LocalDate.of(2019, 5, 01));
        List<Product> productList = List.of(p, jap, ma);
        System.out.println(descendingOrder(productList));

        System.out.println(expiredProducts(productList));
        System.out.println(ingredientsType(productList));

    }

    private static List<Product> expiredProducts(List<Product> listP) {
        List<Product> expiredList = new ArrayList<>();
        for (Product p : listP) {
            if (p.getBestBefore().isBefore(LocalDate.now())) {
                expiredList.add(p);
            }
        }
        return expiredList;
    }

    private static List<Product> ingredientsType(List<Product> list) {
        List<Product> pi = new ArrayList<>();
        for (Product p : list) {
            boolean isVegetarian = true;
            for (Ingredient s : p.getIngredients()) {
                if (s.getType() == IngredientType.ANIMAL) {
                    isVegetarian = false;
                }
            }
            if (isVegetarian) {
                pi.add(p);
            }
        }
        return pi;
    }

    private static List<Product> descendingOrder(List<Product> list) {
        for (Product p : list) {
            p.getIngredients().sort(Comparator.reverseOrder());
        }
        return list;
    }
}