package ro.aniela;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class Exercise41 {
    public static void main(String[]args){
        System.out.println(weekdaysOgMyBirthday(LocalDate.now(), 10));
    }

    private static Map<Integer, DayOfWeek> weekdaysOgMyBirthday(LocalDate current, int year){
        Map<Integer, DayOfWeek> dB= new HashMap<>();
        int currentYear=current.getYear();
        if(LocalDate.of(current.getYear(),2,22).isBefore(current)){
          currentYear++;
        }
        for(int i= currentYear; i<=currentYear+year; i++){
            LocalDate.of(i,2,22).getDayOfWeek();
            dB.put(i,LocalDate.of(i,2,22).getDayOfWeek());
        }
        return dB;
    }
}
