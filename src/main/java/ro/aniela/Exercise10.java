package ro.aniela;

import java.util.Map;

public class Exercise10 {
    public static void main(String[]args){
        //to get the values of a specified key in a Map
        Map<Integer, String> map=Map.of(1,"dkljn", 2, "sh");
        System.out.println(theSpecifiedValues(map, 1));
    }

    private static String theSpecifiedValues(Map<Integer, String> map, Integer key){

       return map.get(key);
    }

}
