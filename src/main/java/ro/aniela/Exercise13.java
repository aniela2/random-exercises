package ro.aniela;

public class Exercise13 {
        public static void main(String[]args){

            Customer c=new Customer(123, "S" , 19);
            Customer n=new Customer(145, "name", 19);
            System.out.println(areEquals(c, n));

        }

        private static boolean areEquals(Customer c, Customer n){
            if( c.equals(n)){
                return c.hashCode() == n.hashCode();
            }
            return false;
        }

    }
