package ro.aniela;

import java.util.ArrayList;
import java.util.List;

public class Exercise29 {
    public static void main(String[]args){
        System.out.println(checkIfUniqueCharacters("111"));


    }

    private static boolean checkIfUniqueCharacters(String given){
        List<String> unique= new ArrayList<>();
        String[]args=given.split("");
        for(String s: args){
            if(!(unique.contains(s))){
                unique.add(s);
            }else{
                return false;
            }
        }
        return true;
    }
}
