package ro.aniela;

public enum Curriencies {
    EUR(Country.ITALIA, Continent.EUROPA),
    USD(Country.ROMANIA, Continent.EUROPA);


    private Continent continent;
    private Country country;

    private Curriencies(Country country, Continent continent) {
        this.country = country;
        this.continent = continent;
    }

    public Continent getContinent() {
        return continent;
    }

    public Country getCountry() {
        return country;
    }

    public String toString() {
        return " Curriencies is: continent " + continent + ", country" + country;
    }
}
