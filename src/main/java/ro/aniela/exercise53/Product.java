package ro.aniela.exercise53;

import ro.aniela.Curriencies;

public class Product {
    // name, currency(Currency enum) and amount
    private String name;
    private Currency currency;
    private int amount;
    public Product(String name, Currency currency, int amount){

        this.name=name;
        this.currency=currency;
        this.amount=amount;
    }

    public String getName() {
        return name;
    }

    public Currency getCurrency() {
        return currency;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", currency=" + currency +
                ", amount=" + amount +
                '}';
    }
}

