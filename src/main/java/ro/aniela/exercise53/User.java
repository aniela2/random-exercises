package ro.aniela.exercise53;

public class User {

    private int id;
    private String name;
    private Country residentCountry;

    public User(int id, String name, Country residentCountry) {

        this.id = id;
        this.name = name;
        this.residentCountry = residentCountry;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Country getResidentCountry() {
        return residentCountry;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", residentCountry=" + residentCountry +
                '}';
    }
}
