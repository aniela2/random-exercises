package ro.aniela.exercise53;

import java.util.List;

public enum Currency {
    USD( List.of(Country.BULGARIA)),
    GBP(List.of(Country.ANGLIA)),
    RON(List.of(Country.ROMANIA));
    //A Currency has an attribute availableFor which is a List of Countries.
    private List<Country> availableFor;

    Currency(List<Country> availableFor) {
        this.availableFor = availableFor;
    }

    public List<Country> getAvailableFor() {
        return availableFor;
    }

    }
