package ro.aniela.exercise53;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class exercise53 {
    public static void main(String[] args) {
        User user=new User(134, "cfg", Country.ROMANIA);
        Product product= new Product("kjj", Currency.GBP, 13);
        Product product1=new Product("hhv",Currency.RON, 12);
        List<Product> products= Arrays.asList(product, product1);
        System.out.println(tryToBy(user, products));

        //id, name, residentCountry(Country enum) wants to pay for some Products.
        //  A Product has the following attributes: name, currency(Currency enum) and amount.
        //A Currency has an attribute availableFor which is a List of Countries.
        //When the User will try to buy some Products, s/he will be allowed to buy
        // only the Products that have a Currency available for her/his Country.
        // He will also be notified which Products was not allowed to buy.
    }

    private static Map<Boolean, List<Product>> tryToBy(User u, List<Product> products) {
        List<Product> listToBy = new ArrayList<>();
        List<Product> notAllowed= new ArrayList<>();

        for (Product p : products) {
            if (p.getCurrency().getAvailableFor().contains(u.getResidentCountry())) {
                listToBy.add(p);
            } else {
                notAllowed.add(p);
            }
        }

        return Map.of(true , listToBy,false, notAllowed);
    }

}
