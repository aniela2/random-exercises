package ro.aniela;

public class Exercise12 {
    public static void main(String[] args){
        Integer[]integers={1,2,5,6,7};
        System.out.println(sum(integers, 4));

    }

    private static int sum(Integer[] integers, int number){
        int sum=0;
        for(int i: integers){
            if(i>number){
                sum+=i;
            }
        }
        return sum;
    }
}
