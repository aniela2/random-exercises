package ro.aniela;

import ro.aniela.exercise34.Bike;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Exercise47 {
    // Having a List of Buildings (numberOfFloors, name),
    // sort them in reverse order, first by number of floors and second by names.

    public static void main(String[] args) {

        Building b=new Building(1,"Adrian");
        Building c= new Building(2,"MAn");
        Building d= new Building(5, "df");
        Building e= new Building(4,"fdf");
        Building f= new Building(3," dfjdjrh");
        List<Building> list= new ArrayList<>();
        list.add(b);
        list.add(c);
        list.add(d);
        list.add(f);
        list.add(e);
        sortInReverse(list);
    }

    private static void sortInReverse(List<Building> buildingList) {
        buildingList.sort(Comparator.reverseOrder());
        System.out.println(buildingList);
    }

}

