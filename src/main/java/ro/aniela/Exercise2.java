package ro.aniela;

import java.util.List;

public class Exercise2 {

    public static void main(String[] args) {
        List<String> ana = List.of("1", "a", "b");
        System.out.println(checkAList("a", ana, 2));
    }

    private static boolean checkAList(String given, List<String> list, int index) {

        return list.get(index).equals(given);
        //return list.contains(given);

    }

}
