package ro.aniela;

import java.util.Arrays;

public class Exercise22 {
    //Write a Java program to check if the first two characters
    // are matching the last two characters of a given string.

    public static void main(String[] args) {
        System.out.println(theMatchOfTwoString("12asnf12hdhs"));

    }

    private static boolean theMatchOfTwoString(String given) {
        String one = given.substring(0, 1);
        String two = given.substring(given.length() - 2, given.length() - 1);
        return one.equals(two);
    }
}

