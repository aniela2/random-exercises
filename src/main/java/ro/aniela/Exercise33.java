package ro.aniela;

import java.security.PublicKey;

public class Exercise33 {
    public static void main(String[] args) {
        System.out.println(weekDay(8));
    }

    public static String weekDay(int numberOfTheDay) {
        Weekday[] weekdays = Weekday.values();
        for (Weekday w : weekdays) {
            if (w.getNumberOfTheDay() == numberOfTheDay) {
                return w.name();
            }
        }
        return "This is not a day.";
    }
}
