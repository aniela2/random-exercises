package ro.aniela;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Exercise46 {
    public static void main(String[]args){
        List<Integer> list=new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(4);
        System.out.println(reverseSort(list));
    }

    private static List<Integer> reverseSort(List<Integer> list){

        Collections.sort(list, Comparator.reverseOrder());
        return list;
    }
}
