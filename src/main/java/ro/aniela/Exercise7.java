package ro.aniela;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Exercise7 {
    public static void main(String[]args){
        System.out.println(allTheCountry());
        System.out.println(currencies(Continent.EUROPA));
        System.out.println(building());

    }

    private static List<Country> allTheCountry(){
        List<Country> countries=new ArrayList<>();
     for(Curriencies c: Curriencies.values()){
        countries.add(c.getCountry());
     }
     return countries;

    }

    private static List<Curriencies> currencies(Continent continent){
        List<Curriencies> curriencies=new ArrayList<>();
       for(Curriencies c: Curriencies.values()){
           if(c.getContinent()==continent){
               curriencies.add(c);
           }

       }
        return curriencies;
    }

    private static Map<String, Pair> building(){
        Map<String, Pair> map=new HashMap<>();
       for(Curriencies c: Curriencies.values()){
           map.put(c.name(), new Pair(c.getContinent(),c.getCountry()));
       }
       return map;
    }


}
