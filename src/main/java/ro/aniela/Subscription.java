package ro.aniela;

import java.time.LocalDate;

public final class Subscription {
    private final String userld;
    private final LocalDate from;
    private final LocalDate to;
    private final SubscriptionType type;

    public Subscription(String userld,LocalDate from, LocalDate to, SubscriptionType type){
        this.userld=userld;
        this.from=from;
        this.to=to;
        this.type=type;
    }

    public String getUserld() {
        return userld;
    }

    public LocalDate getFrom() {
        return from;
    }

    public LocalDate getTo() {
        return to;
    }

    public SubscriptionType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Subscription{" +
                "userld='" + userld + '\'' +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", type=" + type +
                '}';
    }
}
