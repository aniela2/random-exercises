package ro.aniela;

import java.util.Date;
import java.util.List;

public class Exercise4 {
    public static void main(String[]args){
        Student one=new Student("nbh", 1, 3.5);
        Student two=new Student("hh",2, 10d);
        Student three= new Student("Ion", 3, 9.5);


        List<Student>students=List.of(one, two, three);

        System.out.println(typeOfScore(students));
    }

    private static Pair<Double, Double> typeOfScore (List<Student> students){
        double highest= students.get(0).getMarks();
        double lowest=students.get(0).getMarks();

        for(Student s:students){

            if(s.getMarks()>highest)  {
                highest = s.getMarks();
            }
            if(s.getMarks()<lowest){
                lowest=s.getMarks();
            }

        }
        return new Pair<>(highest, lowest);
    }
}
