package ro.aniela;

public class Exercise5 {
    public static void main(String[] args) {
        System.out.println(concatenate("bun", 3));

    }

    private static StringBuilder concatenate(String given, int numberOfTimes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numberOfTimes; i++) {
            sb.append(given);
        }
        return sb;
    }
}

