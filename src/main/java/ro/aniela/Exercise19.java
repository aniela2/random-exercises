package ro.aniela;

import java.time.LocalDate;

public class Exercise19 {
    public static void main(String[]args){

        System.out.println(leapYEarOrNot(1991));
    }

    private static boolean leapYEarOrNot(int year){
       return LocalDate.of(year, 02, 22).isLeapYear();
    }
}
